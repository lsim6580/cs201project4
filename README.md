Name: 	Luke Simmons

Course:     Cs 201
Program: 	Program 4
Due Date: 	3/15/2015
Description: 	We want a program that will solve a maze recursively. We cannot use any loops
Inputs: 	inputs will be the output and the input file.
Outputs: 	We will output the maze in the output file the user has provided.
Algorithm: 	
1. ask the user for the input file.
1a.ask the user for the output file.
2. loop through the file and fill an array full of the characters. the array at max will only be 20 x 20.
3. write a recursive function that will solve the maze.
4. the maze will have an item(@) that needs to be found before the finish(!).
5. after the maze has been solved there needs to be a path('.') from the start to the item to the finish.
6. your recursive function should also keep track of the number of turns it takes to solve the maze.
7. at the end your program should output the maze to a txt file.