

/*********************************************

Name: 	Luke Simmons

Course:     Cs 201
Program: 	Program 4
Due Date: 	3/15/2015
Description: 	We want a program that will solve a maze recursively. We cannot use any loops
Inputs: 	inputs will be the output and the input file.
Outputs: 	We will output the maze in the output file the user has provided.
Algorithm: 	
1. ask the user for the input file.
1a.ask the user for the output file.
2. loop through the file and fill an array full of the characters. the array at max will only be 20 x 20.
3. write a recursive function that will solve the maze.
4. the maze will have an item(@) that needs to be found before the finish(!).
5. after the maze has been solved there needs to be a path('.') from the start to the item to the finish.
6. your recursive function should also keep track of the number of turns it takes to solve the maze.
7. at the end your program should output the maze to a txt file.



*********************************************/


#include <iostream>
#include <string>
#include <fstream>
using namespace std;
char maze[20][20];

bool solveMaze(int xpos, int ypos, char itemwewant,char beentherechar, int &noofturns)//accepts the starting x and y position both ints.
// also accepts the character that we want to find in the maze, a character that we will use to keep track of where we have been.
// and finally it will accept and int passed by reference to keep track of the number of turns.
{
    char item = '@';
    char finish = '!';
    char start = 'o';
    char wall = '*';
    char beenthere1 = '+';
    char beenthere2 = 'x';
    int i, k;
 
    if (maze[xpos][ypos] == wall)// checks to see if we are in a wall
        return false;
    else
    {
        if (maze[xpos][ypos] == itemwewant)// checks to see if we are on the item that we want.
            return true;
        else if (maze[xpos][ypos] == beentherechar)// checks to see if we are in a position that we have been to already
               return false;
        else if ((maze[xpos][ypos] == finish && itemwewant == '@') || (maze[xpos][ypos] == item && itemwewant == '!'))// checks to make sure that we dont mark over out item and ending character.
               {
               }
        else
              {
            if (!(maze[xpos][ypos] == 'o')&& !(maze[xpos][ypos] == '@'))
                maze[xpos][ypos] = beentherechar;
              }

    }
    
    if (solveMaze(xpos + 1, ypos, itemwewant, beentherechar, noofturns) == true)// go right
    {
        noofturns += 1;
        return true;
    }
    else if (solveMaze(xpos, ypos + 1, itemwewant, beentherechar, noofturns) == true)// go down
    {
        noofturns += 1;
        return true;
    }
    else if (solveMaze(xpos - 1, ypos, itemwewant, beentherechar, noofturns) == true)// go left
    {
        noofturns += 1;
        return true;
    }
    else if (solveMaze(xpos, ypos - 1, itemwewant, beentherechar, noofturns) == true)// go up
    {
        noofturns += 1;
        return true;
    }

    else
    {
        if (!(maze[xpos][ypos] == '!') &&!( maze[xpos][ypos] == '@')&& !( maze[xpos][ypos] == 'o'))// erases the places we have been characters if the functions returns false
            maze[xpos][ypos] = ' ';
            return false;
    }
}
int main()
{
    string input, output;// input and output files
    cout << "please enter the input file ";
    cin >> input;// gets the input file
    cout << endl;
    cout << "please enter the output ";
    cin >> output;// gets the output file
    cout << endl;
	ifstream fin(input);
    if (!fin.good())// checks to see if the file is available
    {
        cout << "File not found";
        system("pause");
        return 0;
    }
	ofstream fout(output);
	bool specialItemfound = false;
	int numberofturns=0;
	int row;
	int column;
	int i;
	int k;
	int startingY = 0;
	int startingX = 0;
    int startingitemX = 0;
    int startingitemY = 0;
	fin >> column;// gets the number of rows from the input file
	fin >> row;// gets the number of columns from the input file
	fin.get();// will get the '\n' and ignore the character
	for (i = 0; i < row; i++)// will loop through and get place the characters in the array
	{
		for (k = 0; k < column; k++)
		{
			fin.get(maze[i][k]);
			if (maze[i][k] == 'o')// finds our starting position.
			{
				startingX = i;
				startingY = k;
			}
            else if (maze[i][k] == '@')// finds our item coordinates
            {
                startingitemX = i;
                startingitemY = k;
            }
		}
		fin.get();// gets out the newline characters 

	}
	solveMaze(startingX, startingY, '@','+', numberofturns);// call the function to find the item.
    solveMaze(startingitemX, startingitemY, '!', 'X', numberofturns);// call the function to find the finish.
    for (i = 0; i < 20; i++)// replaces the characters within our array that we have placed there to keep track of where we have been
    {
        for (k = 0; k < 20; k++)
        {
            if (maze[i][k] == '+' || maze[i][k] == 'X')
                maze[i][k] = '.';
        }
    }
    for (i = 0; i < 20; i++)// outputs the array into text file. and makes the legend.
    {
        for (k = 0; k < 20; k++)
        {
            fout << maze[i][k];
            if (i == 0 && k == 19)
                fout << '\t' << "LEGEND";
            if (i == 1 && k == 19)
                fout << '\t' << "* - wall";
            if (i == 2 && k == 19)
                fout << '\t' << "@ - item";
            if (i == 3 && k == 19)
                fout << '\t' << "o - starting position";
            if (i == 4 && k == 19)
                fout << '\t' << "! - ending position";
            if (i == 5 && k == 19)
                fout << '\t' << ". - winning path";
            if (i == 6 && k == 19)
                fout << '\t' <<"Total number of moves " << numberofturns;
        }
        fout << endl;
    }
    fout.close();
    fin.close();
   
}